FROM node:14-alpine

WORKDIR /app

RUN npm install -g nodemon mocha

COPY package.json package.json
RUN npm install

COPY . .

LABEL maintainer="John Kevin Losito"

CMD ["node", "server.js"]
