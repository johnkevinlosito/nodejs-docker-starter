# Hello World NodeJS

Starter package for dockerizing a simple NodeJS app

## Getting started

To start project, run:

```
docker-compose up
```

The app will then be available at http://localhost:5000
